import { defineConfig } from "vitest/config";

export default defineConfig({
    test: {
        poolOptions: {
            vmThreads: {
                memoryLimit: "50%"
            }
        }
    }
});
