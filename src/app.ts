/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

// Initiate all services and web server

"use strict";

import express, { type RequestHandler, type Application } from "express";

import { deployDatabase } from "./database/init";
import { refereesRouter } from "./referees/refereesRoutes";

// DATABASE
deployDatabase();

// EXPRESS
export const application: Application = express();
export const router = express.Router();

// MIDDLEWARE
application.use(express.json());

// ROUTES
const rootRoute: RequestHandler = (_, res) => {
    const data = { message: "Apito | Evaluate soccer referees' performance." };

    res.status(200);
    res.send(data);
};

const openAPIRoute: RequestHandler = (_, res) => {
    const data = { message: "Apito | openAPI" };

    res.status(200);
    res.send(data);
};

application.get("", rootRoute);
router.get("", openAPIRoute);
router.use("/referees", refereesRouter);

application.use("/api/v1", router);

// ROUTES - INVALID ROUTES
application.use((_, res, next) => {
    res.status(404);
    res.type("json");
    res.send({ message: "Error, path not found!" });

    next();
});
