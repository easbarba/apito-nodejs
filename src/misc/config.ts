/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import process from "process";

const HOSTNAME = process.env.HOSTNAME ?? null;
const PORT = process.env.PORT ?? null;

export const ports: { hostname: string; port: number } = {
    hostname: HOSTNAME ?? "0.0.0.0",
    port: PORT !== null ? parseInt(PORT) : 8080
};

export const pagination: { limit: number; offset: number } = {
    limit: 0,
    offset: 10
};
