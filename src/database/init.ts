/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import { PrismaClient } from "@prisma/client";

import { createReferees } from "../referees/refereesFactory";

export const dbClient = new PrismaClient();

export async function connectDatabase(): Promise<void> {
    console.log("Database connected");
    await dbClient.$connect();
}

export async function seedDatabase(): Promise<void> {
    createReferees(dbClient).catch(async (e) => {
        console.error(e);
        await disconnectDatabase();
        process.exit(1);
    });
}

export async function disconnectDatabase(): Promise<void> {
    console.log("Database disconnected");
    await dbClient.$disconnect();
}

export function deployDatabase(): void {
    connectDatabase().catch(async (e) => {
        console.error(e);
        await disconnectDatabase();
        process.exit(1);
    });

    seedDatabase().catch(async (e) => {
        console.error(e);
        await disconnectDatabase();
        process.exit(1);
    });
}
