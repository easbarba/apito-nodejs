/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import express from "express";

import { RefereesController } from "./refereesController";

const refereesController = new RefereesController();
export const refereesRouter = express.Router();

refereesRouter
    .route("/")
    .get(refereesController.index)
    .post(refereesController.new);

refereesRouter
    .route("/:id")
    .get(refereesController.show)
    .put(refereesController.update)
    .patch(refereesController.updatePartial)
    .delete(refereesController.destroy);
