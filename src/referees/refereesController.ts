/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import { type NextFunction, type Request, type Response } from "express"; // type Request
import { StatusCodes } from "http-status-codes";

import { parsePaginationParams } from "../misc/utils";
import { type Referee } from "./referee";
import { RefereesRepository } from "./refereesRepository";
import {
    type TypedRequest,
    type TypedRequestBody,
    type TypedRequestParams,
    type TypedRequestQuery
} from "../common/request";
import { ResponseDTO } from "../common/responseDTO";
const refereeRepository = new RefereesRepository();

export class RefereesController {
    index = async (
        req: TypedRequestQuery<{ limit: string; offset: string }>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        const { limit, offset } = parsePaginationParams(
            req.query.limit,
            req.query.offset
        );
        try {
            const referees = await refereeRepository.getAll(limit, offset);

            res.status(StatusCodes.OK).json(
                new ResponseDTO<Referee[]>("success", referees)
            );
        } catch (err) {
            res.status(StatusCodes.NOT_FOUND).json(
                new ResponseDTO<any>("fail", err)
            );

            next();
        }
    };

    show = async (
        req: TypedRequestParams<{ id: string }>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const referee = await refereeRepository.getOne(req.params.id);

            res.status(StatusCodes.OK).json(
                new ResponseDTO<Referee>("success", referee)
            );
        } catch (err) {
            res.status(StatusCodes.NOT_FOUND).json(
                new ResponseDTO<any>("fail", err)
            );
            next();
        }
    };

    new = async (
        req: TypedRequestBody<Referee>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const data: Referee = {
                name: req.body.name,
                state: req.body.state
            };

            const found = await refereeRepository.getByName(data.name);
            if (found !== null) {
                res.status(StatusCodes.BAD_REQUEST).json(
                    new ResponseDTO<any>(`referee $referee already exist!`, "")
                );
            }

            const referee = await refereeRepository.save(data);
            res.status(StatusCodes.CREATED).json(
                new ResponseDTO<Referee>("success", referee)
            );
        } catch (err) {
            res.status(StatusCodes.NOT_MODIFIED).json(
                new ResponseDTO<any>("fail", err)
            );
            next();
        }
    };

    update = async (
        req: TypedRequest<{ referee: Referee }, { id: string }>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const referee = await refereeRepository.update(
                req.body.referee,
                req.params.id
            );

            res.status(StatusCodes.OK).json(
                new ResponseDTO<Referee>("success", referee)
            );
        } catch (err) {
            res.status(StatusCodes.NOT_FOUND).json(
                new ResponseDTO<any>("fail", err)
            );
            next();
        }
    };

    updatePartial = async (
        req: TypedRequest<{ referee: Referee }, { id: string }>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const referee = await refereeRepository.update(
                req.body.referee,
                req.params.id
            );

            res.status(StatusCodes.OK).json(
                new ResponseDTO<Referee>("success", referee)
            );
        } catch (err) {
            res.status(StatusCodes.NOT_FOUND).json(
                new ResponseDTO<any>("fail", err)
            );
            next(err);
        }
    };

    destroy = async (
        req: TypedRequestParams<{ id: string }>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            await refereeRepository.delete(req.params.id);
            res.status(StatusCodes.NO_CONTENT);
        } catch (err) {
            res.status(StatusCodes.NOT_FOUND).json(
                new ResponseDTO<any>("fail", err)
            );
            next(err);
        }
    };
}
