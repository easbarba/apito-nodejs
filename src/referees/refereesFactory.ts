/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import { type PrismaClient, type Prisma } from "@prisma/client";
import { type DefaultArgs } from "@prisma/client/runtime/library";

import { type Referee } from "./referee";
import { getData } from "../database/common";

export async function createReferees(
    dbClient: PrismaClient<Prisma.PrismaClientOptions, never, DefaultArgs>
): Promise<any> {
    console.log("-> Seeding referees");
    const data = await getReferees();
    if (data === null) {
        console.log("No data has been found!");
        return;
    }

    await dbClient.referees.createMany({
        data,
        skipDuplicates: true
    });
}

async function getReferees(): Promise<Referee[] | null> {
    try {
        const data = await getData();
        const refereesList: Array<[string, string]> = Object.entries(
            data.referees
        );
        const referees: Referee[] = refereesList.map((value) => {
            const referee: Referee = {
                name: value[0],
                state: value[1]
            };

            return referee;
        });

        return referees;
    } catch (err) {
        console.log(err);
    }

    return null;
}
