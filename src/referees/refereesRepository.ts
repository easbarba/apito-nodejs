/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import { dbClient } from "../database/init";
import { type Referee } from "./referee";

export class RefereesRepository {
    async getByName(name: string): Promise<any> {
        return await dbClient.referees.findFirst({ where: { name } });
    }

    async getAll(offset: number, limit: number): Promise<any> {
        return await dbClient.referees.findMany({
            skip: offset,
            take: limit
        });
    }

    async getOne(id: string): Promise<any> {
        return await dbClient.referees.findUnique({
            where: { id }
        });
    }

    async update(referee: Referee, id: string): Promise<any> {
        return await dbClient.referees.update({
            where: { id },
            data: referee
        });
    }

    async updatePartial(referee: Referee, id: string): Promise<any> {
        return await dbClient.referees.update({
            where: { id },
            data: referee
        });
    }

    async save(referee: Referee): Promise<any> {
        return await dbClient.referees.create({ data: referee });
    }

    async delete(id: string): Promise<any> {
        await dbClient.referees.delete({ where: { id } });
    }
}
