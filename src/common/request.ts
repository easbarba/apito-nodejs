/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import { type Params, type Query } from "express-serve-static-core";

export interface TypedRequestBody<B> extends Express.Request {
    body: B;
}

export interface TypedRequestQuery<Q extends Query> extends Express.Request {
    query: Q;
}

export interface TypedRequestParams<P extends Params> extends Express.Request {
    params: P;
}

export interface TypedRequest<B, P extends Params> extends Express.Request {
    body: B;
    params: P;
}
