# MODELS

## Referee

- name
- state
- red flags
- yellow flags
- errors
- League[]
- Match[]

## Team

- name
- age
- state
- League[]
- Match[]

## Match

- result
- League
- Referee
- Team[2]

## League

- name
- Team[]
- fixtures

## User

- name
- password
- email
