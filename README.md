<!-- 
 apito-express is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 apito-express is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with apito-express. If not, see <https://www.gnu.org/licenses/>.
-->

# Apito | Express.js

Evaluate soccer referees' performance.

[Vue.js](https://gitlab.com/easbarba/apito-vue) | [Quarkus](https://gitlab.com/easbarba/apito-quarkus) | [Main](https://gitlab.com/easbarba/apito) 

## [Documentation](docs)

All information about API design, openAPI, and related documentation can be found in the `docs`.

## [Makefile](Makefile)

To speed up development some `make` targets are provided. 

| targets          | description                                           |
|------------------|-------------------------------------------------------|
| up               | spin up containers to development (synced folders)    |
| down             | shutdown spinned containers                           |
| exec             | run commands inside  development container            |
| build            | build development container image                     |
| publish          | push to registry current development  container image |
| test.integration | run integration tests                                 |
| test.unit        | run unit tests                                        |

PS: It relies on an `.envs` directory, so be careful to export its content before running its goals.

## [Bin Folder](bin)

There are some handy scripts to easily perform daily tasks, check out.

## Development

## [Podman Pods](https://podman.io)

Podman's pod offers a rootless k8s's pods like experience to local development, [check out!](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods#shortcut_to_create_pods)

Check the `Makefile` file for examples on how to use it.

![podman pod](podman_pod.png)

For more information on development check out the `CONTRIBUTING.md` document.

## [apitest](apitest)

`apitest` is a simple cli script that leverages some UNIX tools to mimic `insomnia` features, as so we can test all endpoints easily.

    ./apitest
    ./apitest 29fe4068-fdf8-4775-acc0-140c4d066612

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
