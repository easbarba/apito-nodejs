/*
 *  apito-express is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-express is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-express. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import { afterAll, beforeAll, expect, test } from "vitest";
import request from "supertest";
import { StatusCodes } from "http-status-codes";

import { application } from "../../src/app";
import {
  seedDatabase,
  disconnectDatabase,
  dbClient,
  connectDatabase
} from "../../src/database/init";

beforeAll(async () => {
  await connectDatabase();
  await seedDatabase();
});

afterAll(async () => {
  const deleteReferees = dbClient.referees.deleteMany();

  await dbClient.$transaction([deleteReferees]);
  await disconnectDatabase();
});

test("should return all referees", async () => {
  const response = await request(application).get("/api/v1/referees");

  expect(response.statusCode).toBe(StatusCodes.OK);
  expect(response.body.data.length).toBe(10);
});

test("should return one referee", async () => {
  const referee = await dbClient.referees.findFirst();
  const response = await request(application).get(
    `/api/v1/referees/${referee?.id}`
  );

  expect(response.statusCode).toBe(StatusCodes.OK);
  expect(response.body.data.name).toBe(referee?.name);
});

test("should return error for inexistent referee", async () => {
  const response = await request(application).get(`/api/v1/referees/0`);
  expect(response.statusCode).toBe(StatusCodes.NOT_FOUND);
});

test("should partially update a referee", async () => {
  const referee = await dbClient.referees.findFirst();
  const response = await request(application).patch(
    `/api/v1/referees/${referee?.id}`
  );

  expect(response.statusCode).toBe(StatusCodes.NO_CONTENT);
  expect(Object.keys(response.body).length).toBe(0);
});

test("should delete a referee", async () => {
  const referee = await dbClient.referees.findFirst();
  const response = await request(application).delete(
    `/api/v1/referees/${referee?.id}`
  );

  expect(response.statusCode).toBe(StatusCodes.OK);
  expect(Object.keys(response.body.data.name).length).toBe("Bruno Henrique");
});
