# apito-express is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-express is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-express. If not, see <https://www.gnu.org/licenses/>.

FROM node:20 AS base
WORKDIR /app

FROM base AS install
WORKDIR /deps
COPY package.json package-lock.json .
RUN npm install

FROM base as final
MAINTAINER EAS Barbosa <easbarba@outlook.com>
WORKDIR /app
COPY --from=install /deps/node_modules node_modules
COPY . .
CMD ["npm", "run", "test:unit"]
