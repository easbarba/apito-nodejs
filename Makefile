# apito-express is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-express is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-express. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include ./envs/*

NAME := apito-express
VERSION := $(shell awk '/version/ {line=substr($$2, 2,5); print line}' ./package.json)
FULLNAME= ${USER}/${NAME}:${VERSION}
RUNNER ?= podman
BACKEND_IMAGE=${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER=/app

# ================================= MAIN COMMANDS
.PHONY: up
up: initial database server

.PHONY: down
down:
	${RUNNER} pod rm --force --ignore ${POD_NAME}
	${RUNNER} container rm --force --ignore ${NAME}
	${RUNNER} container rm --force --ignore ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}

.PHONY: stats
stats:
	${RUNNER} pod stats ${POD_NAME}

# ================================= POD

.PHONY: initial
initial:
	# ----------- CREATE POD
	${RUNNER} pod create \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--name ${POD_NAME}

.PHONY: database
database:
	# ----------- ADD DATABASE
	${RUNNER} rm -f ${DATABASE_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE} \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

.PHONY: database.repl
database.repl:
	# ----------- DATABASE REPL
	${RUNNER} exec -it ${DATABASE_NAME} \
		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE}

.PHONY: database.test
database.test:
	# ----------- ADD DATABASE TESTING
	${RUNNER} rm -f ${DATABASE_NAME}-test
	${RUNNER} volume rm -f ${DATABASE_DATA}-test

	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME}-test \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE}_test \
		--volume ${DATABASE_DATA}-test:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

# database.test.old:
# 	${RUNNER} exec -it ${DATABASE_NAME} \
# 		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE} \
# 			--command 'drop database if exists ${SQL_DATABASE}_test'
# 	${RUNNER} exec -it ${DATABASE_NAME} \
# 		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE} \
# 			--command 'create database ${SQL_DATABASE}_test'

.PHONY: server
server:
	# ----------- SERVER
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--restart=unless-stopped \
		--name ${SERVER_NAME} \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--volume ./ops/nginx-default.conf:/etc/nginx/conf.d/default.conf:Z \
		${SERVER_IMAGE}

.PHONY: prod
prod:
	${RUNNER} container rm -f ${NAME}-prod
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-prod \
		--volume ${PWD}:/app \
		--workdir /app \
		${BACKEND_IMAGE}

.PHONY: start
start:
	${RUNNER} container rm -f ${NAME}-start
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-start \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE} \
		${BACKEND_IMAGE} \
		bash -c 'npm install && npm run db:seed && npm run start:dev'

.PHONY: repl
repl:
	${RUNNER} container rm -f ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-repl \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE} \
		${BACKEND_IMAGE} \
		bash

.PHONY: test
test: database.test
	@${RUNNER} container rm -f ${NAME}-test
	@${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}_test \
		${BACKEND_IMAGE} \
		bash -c 'npm install && npm run db:seed && npm run test:reload'


.PHONY: test.integration
test.integration: database.test
	@${RUNNER} container rm -f ${NAME}-test
	@${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}_test \
		${BACKEND_IMAGE} \
		bash -c 'npm install && npm run db:seed && npm run test:integration'

.PHONY: test.unit
test.unit: database.test
	@${RUNNER} container rm -f ${NAME}-test
	@${RUNNER} container rm -f ${DATABASE_NAME}-test
	@${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}_test \
		${BACKEND_IMAGE} \
		bash -c 'npm install && npm run db:seed && npm run test:integration'

.PHONY: command
command:
	@${RUNNER} container rm -f ${NAME}-command
	@${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-command \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE} \
		${BACKEND_IMAGE} \
		bash -c '$(shell cat container-commands | fzf)'

.PHONY: api
api:
	@./apitest | jq

.PHONY: api.id
api.id:
	@./apitest ${id} | jq

.PHONY: build
build:
	# ---------------------- BUILD BACKEND IMAGE
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}

.PHONY: publish
publish:
	# ---------------------- PUBLISH BACKEND IMAGE
	${RUNNER} push ${BACKEND_IMAGE}

.DEFAULT_GOAL := test
